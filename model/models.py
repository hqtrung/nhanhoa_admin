# -*- coding: utf-8 -*-
from openerp import models, fields, api,_
from openerp.exceptions import UserError, RedirectWarning, ValidationError, except_orm
import time, datetime
from dateutil.relativedelta import relativedelta
from operator import itemgetter


class apartment_building(models.Model):
    _name = "apartment.building"
    _inherit = ['mail.thread', 'ir.needaction_mixin']


    
    def _count_room(self):
        for record in self:
            record.room = len(self.room_ids)
            
            
            
    name = fields.Char("Tên")
    floor = fields.Integer('Số tầng')
    room = fields.Char(compute='_count_room')
    room_ids =  fields.One2many('apartment.room','building_id', string='Danh sách phòng')
    description = fields.Text('Ghi chú')


class apartment_room(models.Model):
    _name = "apartment.room"
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    name = fields.Char("Tên phòng",required=True)
    building_id = fields.Many2one('apartment.building', 'Tòa nhà',required=True)
    description = fields.Text('Ghi chú')
    water_heater = fields.Boolean('Máy nước nóng')
    mezzanine = fields.Boolean('G�c l?ng')
    air_condition = fields.Boolean('Máy lạnh')
    product_id = fields.Many2one('product.product', 'Tiền phòng', required=True)
    price = fields.Float(related='product_id.list_price', string='Giá phòng', store=True, readonly=True) 

