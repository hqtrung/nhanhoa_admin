# -*- coding: utf-8 -*-
from openerp import models, fields, api
from openerp.exceptions import except_orm
import time

class Tenant(models.Model):
    _name = 'apartment.tenant'

    partner_id = fields.Many2one('res.partner', "Khách hàng", required = True)
    contract_id = fields.Many2one('apartment.contract', 'Hợp đồng')
    date_start = fields.Date('Ngày bắt đầu', required = True)
    date_end = fields.Date('Ngày kết thúc')
    gui_xe = fields.Selection([
        ('xemay', 'Xe máy'),
        ('xedap', 'Xe đạp')],
        string="Gửi xe tháng", )
    
    
    _defaults = {
        'date_start': time.strftime('%Y-%m-%d'),
    }


    @api.model
    def create(self, vals):
        rs = super(Tenant, self).create(vals)
        cxt = self.env.context
        if cxt['params'].has_key('id'):
            if cxt['params']['id']:
                rs.contract_id = cxt['params']['id']
        return rs