# -*- coding: utf-8 -*-
from openerp import models, fields, api,_
from openerp.exceptions import UserError, RedirectWarning, ValidationError, except_orm
import time, datetime
from dateutil.relativedelta import relativedelta
from operator import itemgetter


class apartment_service(models.Model):
    _name = 'apartment.service'
    _order = 'date desc'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    """
        Module cho phép người dùng nhập dịch vụ sử dụng cho tòa nhà
    """
    name = fields.Char(compute='_get_name')
    room_id = fields.Many2one('apartment.room', "Phòng")
    contract_id = fields.Many2one('apartment.contract', "Hợp đồng")
    date = fields.Date('Ngày ghi dịch vụ', required=True)
    product_id = fields.Many2one('product.product', 'Dịch vụ', required=True, readonly=True)
    price = fields.Float('Tổng tiền')
    description = fields.Text('Ghi chú')
    is_paid = fields.Boolean('Đã thanh toán')
    state = fields.Selection([
        ('draft', 'Đã ghi nhận'),
        ('confirm', 'Đã thanh toán'),
        ('cancel', 'Đã hủy')], 'Trạng thái')
    _defaults = {
        'state': 'draft',
        'date': time.strftime('%Y-%m-%d'),
        'is_paid': False,
        'product_id': lambda self, cr, uid, c: self.pool.get('ir.model.data').get_object_reference(cr, uid, 'QL_chung_cu', 'price_service')[1]
        
    }
    
    
    @api.one
    def unlink(self):
        if self.state == 'confirm':
            raise except_orm('Lỗi!',
                                 'Không thể xóa dịch vụ đã được thanh toán')
        res = super(apartment_service, self).unlink()
        return res
    
    
    @api.multi
    def _get_name(self):
        for record in self:
            if record.room_id:
                record.name = record.room_id.name + ' - ' + record.product_id.name
            elif record.contract_id:
                record.name = record.contract_id.name + ' - ' + record.product_id.name
            else:
                record.name = record.product_id.name

    @api.one
    def action_confirm(self):
        self.state = 'confirm'

    @api.one
    def action_cancel(self):
        self.state = 'cancel'


