# -*- coding: utf-8 -*-
from openerp import models, fields, api

class product_product(models.Model):
    _inherit = 'product.product'

    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        context = self._context or {}
        if 'room_product' in context:
            categ = self.env.ref('QL_chung_cu.room_product')
            args += [('categ_id', '=', categ.id)]
        return super(product_product, self).search(args, offset, limit, order, count=count)
