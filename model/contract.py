# -*- coding: utf-8 -*-
from openerp import models, fields, api
from openerp.exceptions import except_orm, Warning, RedirectWarning,UserError
import time,datetime
from dateutil.relativedelta import relativedelta


class apartment_contract(models.Model):
    _name = "apartment.contract"
    _order = 'date_created desc'
    _inherit = ['mail.thread', 'ir.needaction_mixin']


    name = fields.Char('Số hợp đồng', readonly=True,copy=False)
    room_id = fields.Many2one('apartment.room', "Phòng", required=True, states={'confirm': [('readonly', True)],'done': [('readonly', True)]})
    price = fields.Float(related='room_id.product_id.list_price', string='Giá phòng', store=True, readonly=True) 
    deposit = fields.Float('Số tiền đặt cọc', states={'confirm': [('readonly', True)],'done': [('readonly', True)]})
    date_created = fields.Date('Ngày tạo hợp đồng',states={'confirm': [('readonly', True)],'done': [('readonly', True)]})
    date_start = fields.Date('Ngày bắt đầu hợp đồng', required=True,states={'confirm': [('readonly', True)],'done': [('readonly', True)]})
    date_end = fields.Date('Ngày kết thúc hợp đồng',required=True,states={'confirm': [('readonly', True)],'done': [('readonly', True)]})
    water_start = fields.Float('Chỉ số nước ban đầu', required=True, states={'confirm': [('readonly', True)],'done': [('readonly', True)]})
    power_start = fields.Float('Chỉ số điện ban đầu', required=True, states={'confirm': [('readonly', True)],'done': [('readonly', True)]})
    gui_xe = fields.Selection([('xemay', 'Xe máy'),('xedap', 'Xe đạp')],string="Gửi xe tháng", states={'confirm': [('readonly', True)],'done': [('readonly', True)]})
    supplier_id = fields.Many2one('res.partner', 'Khách hàng', required=True)
    base_service_ids = fields.One2many('apartment.service','contract_id', string='Dịch vụ cơ bản')
    invoice_ids = fields.One2many('account.invoice', 'contract_id')
    tenant_ids = fields.One2many('apartment.tenant', 'contract_id')
    state = fields.Selection([
        ('draft', 'Nháp'),
        ('confirm', 'Xác nhận'),
        ('cancel', 'Đã hủy'),
        ('done', 'Đã kết thúc')], 'Trạng thái')
    _defaults = {
        'state': 'draft',
        'date_created': time.strftime('%Y-%m-%d'),
        'date_start': time.strftime('%Y-%m-%d'),
        'date_end': datetime.datetime.now() + relativedelta(years=1)
    }
    
    @api.one
    def create_tenant(self):
        """
        Khi xác nhận hợp đồng thì đưa người ký hợp đồng vào làm tenant chính
        """
        vals = {
                    'partner_id': self.supplier_id.id,
                    'date_start': self.date_start,
                    'date_end': self.date_end,
                    'gui_xe': self.gui_xe
                }
        if type(self.id) is int:
            vals.update({'contract_id': self.id})
        return self.env['apartment.tenant'].create(vals)
    
    @api.one
    def create_base_service(self):
        res = []
        categ = self.env.ref('QL_chung_cu.default_product')
        args = ['|',('categ_id', '=', categ.id),('id','=',self.room_id.product_id.id)]
        default_product = self.env['product.product'].search(args)
        date_start = self.date_start
        for s in default_product:
            vals = {
                        'product_id': s.id,
                        'date': date_start,
                        'price': s.list_price,
                    }
            if type(self.id) is int:
                vals.update({'contract_id': self.id})
            res.append(self.env['apartment.service'].create(vals))
        return res
    
    
    @api.one
    def unlink(self):
        if self.state == 'confirm':
            raise except_orm('Lỗi!',
                                 'Không thể xóa hợp đồng đã được xác nhận')
        res = super(apartment_contract, self).unlink()
        return res
    
    @api.model
    def create(self, vals):
        print vals
        context = self.env.context
        sohopdong = self._get_name_contract(vals['date_start'], vals['room_id'])
        vals.update({'name':sohopdong})
        
        if vals['date_start'] >= vals['date_end']:
            raise except_orm(u'Lỗi!', u'Ngày kết thúc hợp đồng không được phép nhỏ hơn (hoặc bằng) ngày bắt đầu!')
        
        month_index_vals = {
                            'room_id':vals['room_id'],
                            'date': vals['date_start'],
                            'water_number':vals['water_start'],
                            'power_number':vals['power_start'],
                            'state': 'unpaid'
                            }
        self.env['apartment.month.index'].create(month_index_vals)
        
        new_contract = super(apartment_contract, self).create(vals)
        if 'active_id' in context:
            wizard = self.env['create.contract'].browse(context['active_id']).read_wizard()
            self.env['apartment.tenant'].browse(wizard['tenant_ids']).write({'contract_id':new_contract.id})
            self.env['apartment.service'].browse(wizard['base_service_ids']).write({'contract_id':new_contract.id})
            new_contract.confirm_contract()
            
        if 'from_wizard' not in context:
            new_contract.create_tenant()
            new_contract.create_base_service()
        
        return new_contract
    
    
    def _get_currency(self):
        user_obj = self.env['res.users']
        currency_obj = self.env['res.currency']
        user = user_obj.browse(self.env.uid)
        currency = False
        if user.company_id:
            currency = user.company_id.currency_id.id
        else:
            currency = currency_obj.search([('rate', '=', 1.0)])[0]
        return currency
    
    def _get_name_contract(self,date,room_id):
        sohopdong = 'HD-'
        sohopdong += self.env['apartment.room'].browse(room_id).name
        sohopdong += '-' + date
        return sohopdong
    
    @api.one
    def write(self, vals):
        if 'date_start' in vals and 'room_id' in vals:
            sohopdong = self._get_name_contract(vals['date_start'], vals['room_id'])
            vals.update({'name':sohopdong})
        elif 'date_start' in vals and not 'room_id' in vals:
            sohopdong = self._get_name_contract(vals['date_start'], self.room_id.id)
            vals.update({'name':sohopdong})
        elif not 'date_start' in vals and 'room_id' in vals:
            sohopdong = self._get_name_contract(self.date_start, vals['room_id'])
            vals.update({'name':sohopdong})
            
            
        if 'tenant_ids' in vals:
            list_tenant = [x[0] for x in vals['tenant_ids']]
            if list_tenant.count(2) == len(self.tenant_ids) and list_tenant.count(0) == 0:
                raise except_orm(u'Lỗi!', u'Không được phép xóa hết khách trọ trong hợp đồng!')
        
        if 'base_service_ids' in vals:
            list_service = [x[0] for x in vals['base_service_ids']]
            if list_service.count(2) == len(self.base_service_ids) and list_service.count(0) == 0:
                raise except_orm(u'Lỗi!', u'Không được phép xóa hết dịch vụ cơ bản trong hợp đồng!')
        return super(apartment_contract, self).write(vals)
    
    
    @api.one
    def confirm_contract(self):
        same_room_contract = self.search([('room_id', '=', self.room_id.id),('state', '=', 'confirm')])
        if same_room_contract:
            raise except_orm(u'Lỗi!', u'Phòng %s đang trong một hợp đồng đã được xác nhận' % self.room_id.name)
        
        if len(self.base_service_ids) == 0:
            raise except_orm(u'Lỗi!', u'Vui lòng chọn ít nhất 1 dịch vụ cơ bản (Tiền thuê nhà)')
        
        same_customer_contract = self.search([('supplier_id', '=', self.supplier_id.id),('state', '=', 'confirm')])
        if same_customer_contract:
            raise except_orm(u'Lỗi!', u'%s đang là chủ một hợp đồng đã được xác nhận' % self.supplier_id.name)
        
        #Tạo payment cho đặt cọc
        if self.deposit:
            invoice_obj = self.env['account.invoice']
            
            journal = invoice_obj._default_journal()
            currency = invoice_obj._default_currency()
        
                #Tạo payment trả tiền đặt cọc
            invoice_vals = {
                             u'currency_id': currency.id, #=>OK
                             u'contract_id': self.id, #=>OK
                             u'journal_id': journal.id, #Default => OK
                             u'date_invoice': time.strftime('%Y-%m-%d'), #=>OK
                             u'tax_line_ids': [], #=>OK
                             u'partner_id': self.supplier_id.id, #=>OK
                             u'type': 'out_invoice', #tự thêm
                            }
            
            new_invoice = invoice_obj.new(invoice_vals)
            new_invoice._onchange_partner_id()
            invoice_vals.update({
                                 u'account_id': new_invoice.account_id.id
                                 })
            new_inv_id = invoice_obj.create(invoice_vals)
            deposit_product = self.env.ref('QL_chung_cu.invoice_room')
            self.env['create.invoice'].create_invoice_line(new_inv_id.id, deposit_product.id, self.deposit)

        
        return self.write({'state':'confirm'})
   
   
    @api.one
    def action_cancel(self):
        self.state = 'cancel'
        
    @api.one
    def set_draft_contract(self):
        self.state = 'draft'
    
    @api.one
    def create_invoice(self):
        vals = {'start_date': (datetime.datetime.now() + relativedelta(day=1)),
                'end_date': time.strftime('%Y-%m-%d'),
                'liquid_date': time.strftime('%Y-%m-%d'),
                }
        wizard_id = self.env['create.invoice'].create(vals)
        for c in self.ids:
            wizard_id.create_invoice(contract_id=c)
            
         
class AccountInvoiceRefund(models.TransientModel):
    """Refunds invoice"""

    _inherit = "account.invoice.refund"

    @api.multi
    def compute_refund(self, mode='refund'):
        inv_obj = self.env['account.invoice']
        inv_tax_obj = self.env['account.invoice.tax']
        inv_line_obj = self.env['account.invoice.line']
        context = dict(self._context or {})
        xml_id = False

        for form in self:
            created_inv = []
            date = False
            description = False
            for inv in inv_obj.browse(context.get('active_ids')):
                if inv.state in ['draft', 'proforma2', 'cancel']:
                    raise UserError(_('Cannot refund draft/proforma/cancelled invoice.'))
                if inv.reconciled and mode in ('cancel', 'modify'):
                    raise UserError(_('Cannot refund invoice which is already reconciled, invoice should be unreconciled first. You can only refund this invoice.'))

                date = form.date or False
                description = form.description or inv.name
                refund = inv.refund(form.date_invoice, date, description, inv.journal_id.id)
                refund.compute_taxes()

                created_inv.append(refund.id)
                if mode in ('cancel', 'modify'):
                    movelines = inv.move_id.line_ids
                    to_reconcile_ids = {}
                    to_reconcile_lines = self.env['account.move.line']
                    for line in movelines:
                        if line.account_id.id == inv.account_id.id:
                            to_reconcile_lines += line
                            to_reconcile_ids.setdefault(line.account_id.id, []).append(line.id)
                        if line.reconciled:
                            line.remove_move_reconcile()
                    refund.signal_workflow('invoice_open')
                    for tmpline in refund.move_id.line_ids:
                        if tmpline.account_id.id == inv.account_id.id:
                            to_reconcile_lines += tmpline
                            to_reconcile_lines.reconcile()
                    if mode == 'modify':
                        invoice = inv.read(
                                    ['name', 'type', 'number', 'reference',
                                    'comment', 'date_due', 'partner_id',
                                    'partner_insite', 'partner_contact',
                                    'partner_ref', 'payment_term_id', 'account_id',
                                    'currency_id', 'invoice_line_ids', 'tax_line_ids',
                                    'journal_id', 'date'])
                        invoice = invoice[0]
                        del invoice['id']
                        invoice_lines = inv_line_obj.browse(invoice['invoice_line_ids'])
                        invoice_lines = inv_obj._refund_cleanup_lines(invoice_lines)
                        tax_lines = inv_tax_obj.browse(invoice['tax_line_ids'])
                        tax_lines = inv_obj._refund_cleanup_lines(tax_lines)
                        invoice.update({
                            'type': inv.type,
                            'date_invoice': date,
                            'state': 'draft',
                            'number': False,
                            'invoice_line_ids': invoice_lines,
                            'tax_line_ids': tax_lines,
                            'date': date,
                            'name': description,
                            
                            #Thêm vô contract => vì partner chỉ là related, k contract k tạo invoice đc
                            'contract_id':inv.contract_id.id,
                            'room_id':inv.contract_id.room_id.id,
                        })
                        for field in ('partner_id', 'account_id', 'currency_id',
                                         'payment_term_id', 'journal_id'):
                                invoice[field] = invoice[field] and invoice[field][0]
                        inv_refund = inv_obj.create(invoice)
                        if inv_refund.payment_term_id.id:
                            inv_refund._onchange_payment_term_date_invoice()
                        created_inv.append(inv_refund.id)
                xml_id = (inv.type in ['out_refund', 'out_invoice']) and 'action_invoice_tree1' or \
                         (inv.type in ['in_refund', 'in_invoice']) and 'action_invoice_tree2'
                # Put the reason in the chatter
                subject = _("Invoice refund")
                body = description
                refund.message_post(body=body, subject=subject)
        if xml_id:
            result = self.env.ref('account.%s' % (xml_id)).read()[0]
            invoice_domain = eval(result['domain'])
            invoice_domain.append(('id', 'in', created_inv))
            result['domain'] = invoice_domain
            return result
        return True

  
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            