# -*- coding: utf-8 -*-

from openerp import api, fields, models, _





class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    
    contract_id = fields.Many2one('apartment.contract', string='Contract', states={'draft': [('readonly', False)]})
    room_id = fields.Many2one('apartment.room', related='contract_id.room_id', string='Room', store=True, readonly=True )
    partner_id = fields.Many2one('res.partner', related='contract_id.supplier_id', string='Customer', store=True, readonly=True ,copy=True)
    
    @api.multi
    def invoice_validate(self):
        res = super(AccountInvoice, self).invoice_validate()
        contract_obj = self.env['apartment.contract']
        create_invoice = self.env['create.invoice']
        month_index_obj = self.env['apartment.month.index']
        service_obj = self.env['apartment.service']
        
        #all_contract = contract_obj.search([('state','=','confirm')])
        #for c in all_contract:
        #Cập nhật trạng thái điện nước, dịch vụ
        deposit = self.env.ref('QL_chung_cu.invoice_room')
        if len(self.invoice_line_ids) != 1 and self.invoice_line_ids[0].product_id != deposit.id:
            power_index = create_invoice.get_power_index(self.contract_id.room_id.id)
            power_index = month_index_obj.browse(power_index)
            for p in power_index:
                if p.state == 'draft':
                    p.write({'state':'confirm'})
            
            service_ids = create_invoice.get_all_service(self.contract_id.room_id.id)
            all_service = service_obj.browse(service_ids)
            all_service.write({'state':'confirm'})
        
        return res
    
    @api.multi
    @api.returns('self')
    def refund(self, date_invoice=None, date=None, description=None, journal_id=None):
        new_invoices = self.browse()
        for invoice in self:
            # create the new invoice
            values = self._prepare_refund(invoice, date_invoice=date_invoice, date=date,
                                    description=description, journal_id=journal_id)
            values.update({'contract_id':self.contract_id.id,
                           'room_id':self.contract_id.room_id.id,
                           'partner_id':self.contract_id.supplier_id.id})
            
            new_invoices += self.create(values)
            
        return new_invoices