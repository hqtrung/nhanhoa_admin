# -*- coding: utf-8 -*-

from openerp.osv import fields,osv
from openerp.tools.translate import _


class res_partner(osv.osv):
    _name = 'res.partner'
    _inherit = 'res.partner'


    def _get_contract_name(self, cr, uid, ids, field_name, arg, context=None):
        for p in ids:
            teanant = self.pool('apartment.tenant').search(cr,uid,[('partner_id','=',p)],context=context)
            teanant = self.pool('apartment.tenant').browse(cr,uid,teanant,context=context)
            for t in teanant:
                if t.contract_id.state == 'confirm':
                    return {
                                p : t.contract_id.room_id.name
                            }
        return {
                    p : u'Hết hạn'
                }
        
        
    def open_contract(self, cr, uid, ids, context=None):
        for p in ids:
            teanant = self.pool('apartment.tenant').search(cr,uid,[('partner_id','=',p)],context=context)
            teanant = self.pool('apartment.tenant').browse(cr,uid,teanant,context=context)
            for t in teanant:
                if t.contract_id.state == 'confirm':
                    context = dict(context or {}, 
                                   search_default_room_id=[t.contract_id.room_id.id], 
                                   default_room_id=[t.contract_id.room_id.id],
                                   search_default_state='confirm', 
                                   default_state='confirm')
                    return {
                                'name': _('Contract'),
                                'view_type': 'form',
                                'view_mode': 'tree,form',
                                'res_model': 'apartment.contract',
                                'view_id': False,
                                'type': 'ir.actions.act_window',
                                'context': context,
                            }
        
        
        
    _columns = {
        'full_address': fields.char(string='Full Address', size=200),
        'ssn': fields.char(string='CMND', size=10),
        'birthday': fields.date("Date of Birth"),
        'contract_name': fields.function(_get_contract_name, string='# Contract', type='char'),
    }
    
    
    
    
res_partner()