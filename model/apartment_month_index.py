
# -*- coding: utf-8 -*-
from openerp import models, fields, api,_
from openerp.exceptions import UserError, RedirectWarning, ValidationError, except_orm
import time, datetime
from dateutil.relativedelta import relativedelta
from operator import itemgetter


class apartment_month_index(models.Model):
    _name = "apartment.month.index"
    _order = 'date desc'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    name = fields.Char(compute='_get_name')
    room_id = fields.Many2one('apartment.room', "Phòng", required=True)
    date = fields.Date('Ngày ghi chỉ số',readonly = True, required=True, states={'draft': [('readonly', False)]})
    water_number = fields.Float("Chỉ số nước mới", required=True, states={'draft': [('readonly', False)]})
    power_number = fields.Float("Chỉ số điện mới", required=True, states={'draft': [('readonly', False)]})

    state = fields.Selection([
        ('draft', 'đã ghi nhận'),
        ('confirm', 'đã thanh toán'),
        ('unpaid', 'Không thanh toán'),
        ('cancel', 'đã hủy')], 'Trạng thái')
    _defaults = {
        'is_paid': False,
        'state':'draft',
        'date':time.strftime('%Y-%m-%d'),
    }
    
    @api.one
    def unlink(self):
        if self.state == 'confirm':
            raise except_orm('Lỗi!',
                                 'Không thể xóa chỉ số điện nướcc đã được thanh toán')
        res = super(apartment_month_index, self).unlink()
        return res
    
    def check_diennuoc(self,room_id,date,dien,nuoc):
        old_water=old_power=0
        self.env.cr.execute('''select * 
                        from apartment_month_index
                        where room_id = %s
                            and date <= %s
                        order by date desc, id desc
                        ''',(room_id,date))
        
        thongbao = ''
        power_index = map(itemgetter(0), self.env.cr.fetchall())
        if power_index:
            docs = self.browse(power_index[0])
            old_water = docs.water_number
            old_power = docs.power_number
            thongbao += u'Chỉ số nước cũ: %s, chỉ số điện cũ %s, được ghi nhận ngày: %s'%(docs.water_number,docs.power_number,str(docs.date))
        else:
            contract = self.env['apartment.contract'].search([('room_id', '=', room_id),('state', '=', 'confirm')])
            if contract:
                old_water = contract.water_start
                old_power = contract.power_start
                thongbao += u'Chỉ số nước cũ: %s, chỉ số điện cũ %s, được ghi nhận trong hợp động: %s'%(contract.water_start,contract.power_start,contract.name)
        if (nuoc < old_water or dien < old_power):
            raise UserError(u'Chỉ số điện nước ban đầu không họp lý \n %s'%thongbao)
        return True
        
    @api.model
    def create(self, vals):
        self.check_diennuoc(vals['room_id'], vals['date'], vals['power_number'], vals['water_number'])
        return super(apartment_month_index, self).create(vals)

    def _get_name(self):
        for record in self:
            record.name = self.room_id.name + ' - ' + self.date
            
            
    @api.one
    def action_unpaid(self):
        self.state = 'unpaid'

