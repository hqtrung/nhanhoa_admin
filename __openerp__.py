# -*- coding: utf-8 -*-
{
    'name': 'Quản lý chung cư',
    'version': '1.0',
    'author': 'BaoNguyen',
    'category': 'Quản lý chung cư',
    'description': """ """,
    'depends': [
        'mail',
        'sale',
        'product',
    ],
    'data': [
        #DATA
        'data/product_category.xml',
        'data/cities.xml',
        'data/product.xml',
        'data/cron_auto_liquid.xml',

        
        #WIZARD
        'wizard/create_invoice_view.xml',
        'wizard/create_contract_view.xml',
        
        #REPORT
        
        
        #VIEW
        'view/views.xml',
        'view/contract.xml',
        'view/tenant_view.xml',
        'view/account_invoice_view.xml',
        'view/res_partner_view.xml',
        
        
        
        #MENU
        'view/menu.xml',
        
        
        #SERCURITY
        'security/ir.model.access.csv'
             
        
        
    ],
    'installable': True,
    'application': True,
}
