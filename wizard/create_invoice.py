# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
from openerp import models, fields, api
from openerp.exceptions import except_orm, Warning, RedirectWarning
import datetime
from operator import itemgetter
from dateutil.relativedelta import relativedelta

class CreateInvoice(models.TransientModel):

    _name = "create.invoice"
    
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    liquid_date = fields.Date('Liquidation Date')
    return_deposit = fields.Boolean('Return Deposit')

    _defaults = {
        'end_date': (datetime.datetime.now() + relativedelta(day=1, months=+1, days=-1)),
        'start_date': (datetime.datetime.now() + relativedelta(day=1)),
        'liquid_date': time.strftime('%Y-%m-%d'),
        'return_deposit':True
        
    }
    
    def action_audo_liquidation(self,cr,uid,ids=None,context=None):
        contract_ids = self.pool.get('apartment.contract').search(cr,uid,[('state','=','confirm'),('date_end','=',time.strftime('%Y-%m-%d'))],context=context)
        vals = {'start_date': (datetime.datetime.now() + relativedelta(day=1)),
                'end_date': (datetime.datetime.now() + relativedelta(day=1, months=+1, days=-1)),
                'liquid_date': time.strftime('%Y-%m-%d'),
                }
        wizard_id = self.create(cr,uid,vals,context=context)
        for c in contract_ids:
            self.create_invoice(cr,uid,[wizard_id],contract_id=c,context=context)
    
    @api.multi
    def action_create_invoice(self):
        return self.create_invoice(contract_id=None)
        
    @api.one
    def action_liquidation(self):
        context = self.env.context
        #invoice_obj = self.env['account.invoice']
        
        #journal = invoice_obj._default_journal()
        #currency = invoice_obj._default_currency()
        
        
        wizard_read = self.search_read([('id','in',self.ids)],['return_deposit'])[0]
        if 'active_id' in context:
            contract = self.env['apartment.contract'].browse(context['active_id'])
            #Tạo payment trả tiền đặt cọc
            if contract.deposit and wizard_read['return_deposit']==True:
                for inv in contract.invoice_ids:
                    if inv.amount_total == contract.deposit:
                        inv_refund_obj = self.env['account.invoice.refund']
                        vals = {
                                'date_invoice': time.strftime('%Y-%m-%d'),
                                'description': u'Hoàn tiền cọc',
                                'filter_refund': 'refund'
                                }
                        inv_refund = inv_refund_obj.create(vals)
                        inv_refund.with_context(active_model='account.invoice',
                                                journal_type = 'sale',
                                                active_ids = [inv.id],
                                                type = 'out_invoice',
                                                active_id = inv.id).compute_refund('refund') 
            
            contract.tenant_ids.write({'date_end':time.strftime('%Y-%m-%d')})
            self.create_invoice(contract_id=contract.id)
            contract.date_end = time.strftime('%Y-%m-%d')
            contract.state = 'done'
        else:
            return True
    
    def get_all_service(self,cr,uid,room_id,context=None):
        cr.execute('''select * 
                        from apartment_service 
                        where room_id = %s
                        and state = 'draft'
                        '''%room_id)
            
        service_ids = map(itemgetter(0), cr.fetchall())
        return service_ids
    
    def get_power_index(self,cr,uid,room_id,context=None):
        cr.execute('''select * 
                        from apartment_month_index
                        where room_id = %s
                        and state in ('draft','unpaid')
                        order by date desc,id desc
                        '''%room_id)
            
        power_index = map(itemgetter(0), cr.fetchall())
        return power_index
    
    def create_invoice_line(self,cr,uid,invoice_id,product_id,price,quantity=1, description=None, context=None):
        invoice_line_obj = self.pool.get('account.invoice.line')
        invoice_line_vals = {
                        u'invoice_id':invoice_id,
                         u'product_id': product_id, 
                         u'price_unit': price,
                         u'invoice_line_tax_ids': [], 
                         u'account_analytic_id': False, 
                         u'quantity': quantity, 
                         }
        new_invoice_line = invoice_line_obj.new(cr,uid,invoice_line_vals)
        new_invoice_line._onchange_product_id()
        invoice_line_vals.update({u'uom_id': new_invoice_line.uom_id.id,
                                  u'account_id': new_invoice_line.account_id.id,
                                  u'name':new_invoice_line.name
                                  })
        if description:
            invoice_line_vals.update({u'name':description})
        return invoice_line_obj.create(cr,uid,invoice_line_vals,context=context)
    
    
    def create_invoice(self,cr,uid,ids, contract_id = None, context=None):
        date = self.read(cr, uid, ids, ['start_date','end_date','liquid_date'], context=context)[0]
        #count_day = (datetime.datetime.strptime(date['end_date'], '%Y-%m-%d').date() - datetime.datetime.strptime(date['start_date'], '%Y-%m-%d').date()).days
        real_day = (datetime.datetime.strptime(date['end_date'], '%Y-%m-%d').date() - datetime.datetime.strptime(date['start_date'], '%Y-%m-%d').date()).days
        count_day = ((datetime.datetime.strptime(date['start_date'], '%Y-%m-%d').date() + relativedelta(day=1, months=+1, days=-1)) - (datetime.datetime.strptime(date['start_date'], '%Y-%m-%d').date() + relativedelta(day=1))).days
        print count_day        
        #month = datetime.datetime.strptime(date, '%Y-%m-%d').date().month
        #year = datetime.datetime.strptime(date, '%Y-%m-%d').date().year
        
        
        power_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'QL_chung_cu', 'price_power')
        water_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'QL_chung_cu', 'price_water')
        power_product = self.pool.get('product.product').browse(cr,uid,power_id[1],context=context)
        water_product = self.pool.get('product.product').browse(cr,uid,water_id[1],context=context)
        
        invoice_obj = self.pool.get('account.invoice')
        contract_obj = self.pool.get('apartment.contract')
        service_obj = self.pool.get('apartment.service')
        month_index_obj = self.pool.get('apartment.month.index')
        
        if not contract_id:
            contract_ids = contract_obj.search(cr,uid,[('state','=','confirm')],context=context)
        elif contract_id or 'active_ids' in context:
            contract_ids = context['active_ids']
            #date.update({'end_date':time.strftime('%Y-%m-%d')})
            
        all_contract = contract_obj.browse(cr,uid,contract_ids,context=context)
        
        
        journal = invoice_obj._default_journal(cr,uid,context=None)
        currency = invoice_obj._default_currency(cr,uid,context=None)
        
        #Tao invoice
        for c in all_contract:
            invoice_in_month = invoice_obj.search(cr,uid,[('contract_id','=',c.id),('date_invoice','=',date['start_date']),('date_invoice','<=',date['end_date'])],context=context)
            if not invoice_in_month or contract_id:
                invoice_vals = {
                                 u'currency_id': currency.id, #=>OK
                                 u'contract_id': c.id, #=>OK
                                 u'journal_id': journal.id, #Default => OK
                                 u'date_invoice': time.strftime('%Y-%m-%d'), #=>OK
                                 u'tax_line_ids': [], #=>OK
                                 u'partner_id': c.supplier_id.id, #=>OK
                                 u'type': 'out_invoice', #tự thêm
                                }
                
                new_invoice = invoice_obj.new(cr,uid,invoice_vals)
                new_invoice._onchange_partner_id()
                invoice_vals.update({
                                     u'account_id': new_invoice.account_id.id
                                     })
                new_inv_id = invoice_obj.create(cr,uid,invoice_vals,context=context)
                
                
                #Tính tiền điện nước
                total_water = total_power = 0
                power_index = self.get_power_index(cr, uid, c.room_id.id, context=context)
                for i in range(0,len(power_index)-1):
                    p = month_index_obj.browse(cr,uid,power_index[i],context=context)
                    p_1 = month_index_obj.browse(cr,uid,power_index[i+1],context=context)
                    if p.state == 'draft':
                        total_water += (p.water_number - p_1.water_number)
                        total_power += (p.power_number - p_1.power_number)
                self.create_invoice_line(cr, uid, new_inv_id, power_product.id, power_product.list_price, quantity=total_power,context=context)
                self.create_invoice_line(cr, uid, new_inv_id, water_product.id, water_product.list_price, quantity=total_water,context=context)
                
                
                #Tính tiền dịch vụ phát sinh                
                service_ids = self.get_all_service(cr, uid, c.room_id.id, context=context)
                all_service = service_obj.browse(cr,uid,service_ids,context=context)
                for s in all_service:
                    self.create_invoice_line(cr, uid, new_inv_id, s.product_id.id, s.price, description=s.description, context=context)

                #tính tiền dịch vụ cơ bản      
                if c.date_start <= date['start_date'] and not contract_id: 
                    real_day = (datetime.datetime.strptime(date['end_date'], '%Y-%m-%d').date() - datetime.datetime.strptime(date['start_date'], '%Y-%m-%d').date()).days
                elif c.date_start > date['start_date'] and not contract_id: 
                    real_day = (datetime.datetime.strptime(date['end_date'], '%Y-%m-%d').date() - datetime.datetime.strptime(c.date_start, '%Y-%m-%d').date()).days
                    
                elif c.date_start <= date['start_date'] and contract_id: 
                    real_day = (datetime.datetime.strptime(date['liquid_date'], '%Y-%m-%d').date() - datetime.datetime.strptime(date['start_date'], '%Y-%m-%d').date()).days
                elif c.date_start > date['start_date'] and contract_id: 
                    real_day = (datetime.datetime.strptime(date['liquid_date'], '%Y-%m-%d').date() - datetime.datetime.strptime(c.date_start, '%Y-%m-%d').date()).days    
                
                for bs in c.base_service_ids:
                    quantity = (real_day+1.0)/(count_day+1.0)
                    description = u'Thanh toán %s ngày %s'%(str(real_day+1.0),bs.product_id.name)
                    self.create_invoice_line(cr, uid, new_inv_id, bs.product_id.id, quantity * bs.product_id.list_price,description=description, context=context)
                
                #Tính tiền gửi xe
                xedap = xemay = 0
                for tenant in c.tenant_ids:
                    if tenant.gui_xe == 'xedap':
                        xedap += 1
                    elif tenant.gui_xe == 'xemay':
                        xemay += 1
                xedap_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'QL_chung_cu', 'price_giuxedap')
                xemay_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'QL_chung_cu', 'price_giuxemay')
                xedap_product = self.pool.get('product.product').browse(cr,uid,xedap_id[1],context=context)
                xemay_product = self.pool.get('product.product').browse(cr,uid,xemay_id[1],context=context)
                if xedap:
                    self.create_invoice_line(cr, uid, new_inv_id, xedap_product.id, xedap_product.list_price, quantity=xedap,context=context)
                if xemay:
                    self.create_invoice_line(cr, uid, new_inv_id, xemay_product.id, xemay_product.list_price, quantity=xemay,context=context)
                
        return True
    
    


# {u'comment': False, #=>OK
#      u'currency_id': 24, #=>OK
#      u'date_due': u'2015-12-22', #=>OK
#      u'user_id': 1, 
#      u'contract_id': 1, #=>OK
#      u'partner_bank_id': False, #=>OK
#      u'message_follower_ids': False, #=>OK
#      u'company_id': 1, #=>OK
#      u'name': False, #=>OK
#      u'team_id': 1, 
#      u'journal_id': 1, #Default => OK
#      u'date_invoice': u'2015-12-22', #=>OK
#      u'payment_term_id': False, #=>OK
#      u'tax_line_ids': [], #=>OK
#      u'fiscal_position_id': False, #=>OK
#      u'partner_id': 7, #=>OK
#      u'message_ids': False, #=>OK
#      u'origin': False, #=>OK
#      u'account_id': 7, #Default
#      u'invoice_line_ids': [
#                            [0, False, 
#                             {u'uom_id': 1, 
#                              u'product_id': 6, 
#                              u'sequence': 10, 
#                              u'price_unit': 120, 
#                              u'name': u'G\u1edfi xe s\u1ed1', 
#                              u'discount': 0, 
#                              u'invoice_line_tax_ids': [], 
#                              u'account_analytic_id': False, 
#                              u'quantity': 1, 
#                              u'account_id': 118}], 
#                            
#                            [0, False, 
#                             {u'uom_id': 1, 
#                              u'product_id': 5, 
#                              u'sequence': 11, 
#                              u'price_unit': 1200, 
#                              u'name': u'Ti\u1ec1n \u0111\u1eb7t c\u1ecdc thu\xea nh\xe0', 
#                              u'discount': 0, 
#                              u'invoice_line_tax_ids': [], 
#                              u'account_analytic_id': False, 
#                              u'quantity': 1, 
#                              u'account_id': 118}]
#                            ], 
#      }
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
