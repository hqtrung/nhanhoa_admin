# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
from openerp import models, fields, api
from openerp.exceptions import except_orm, Warning, RedirectWarning
import datetime
from operator import itemgetter
from dateutil.relativedelta import relativedelta

class CreateInvoice(models.TransientModel):

    _name = "create.contract"
    
    name = fields.Char('Số hợp đồng', readonly=True,copy=False)
    price = fields.Float(related='room_id.product_id.list_price', string='Giá phòng', store=True, readonly=True) 
    
    room_id = fields.Many2one('apartment.room', "Phòng", required=False)
    deposit = fields.Float('Số tiền đặt cọc')
    date_created = fields.Date('Ngày tạo hợp đồng')
    date_start = fields.Date('Ngày bắt đầu hợp đồng', required=False)
    date_end = fields.Date('Ngày kết thúc hợp đồng',required=False)
    gui_xe = fields.Selection([('xemay', 'Xe máy'),('xedap', 'Xe đạp')],string="Gửi xe tháng")
    supplier_id = fields.Many2one('res.partner', 'Khách hàng', required=False)
    
    water_start = fields.Float('Chỉ số nước ban đầu', required=False)
    power_start = fields.Float('Chỉ số điện ban đầu', required=False)
    tenant_ids = fields.Many2many('apartment.tenant', string='Tenants') 
    base_service_ids = fields.Many2many('apartment.service',string='Dịch vụ cơ bản')
    #invoice_ids = fields.One2many('account.invoice', 'contract_id')
    #tenant_ids = fields.One2many('apartment.tenant', 'contract_id')



    _defaults = {
        'date_created': time.strftime('%Y-%m-%d'),
        'date_start': time.strftime('%Y-%m-%d'),
        'date_end': datetime.datetime.now() + relativedelta(years=1)
    }
    
    @api.multi
    def temp_contract(self):
        wizard = self.read_wizard()
        contract_obj = self.env['apartment.contract']
        contract_vals = {
                         u'name': contract_obj._get_name_contract(wizard['date_created'],wizard['room_id'][0]), #=>OK
                         u'room_id': wizard['room_id'][0], #=>OK
                         u'supplier_id': wizard['supplier_id'][0], #=>OK
                         u'date_start': wizard['date_start'], #=>OK
                         u'date_end': wizard['date_end'], #=>OK
                         u'gui_xe':wizard['gui_xe']
                        }
                
        return contract_obj.new(contract_vals)
        
    @api.multi
    def read_wizard(self):
        list_field = ['room_id',
                      'supplier_id',
                      'deposit',
                      'date_start',
                      'date_end',
                      'gui_xe',
                      'water_start',
                      'power_start',
                      'tenant_ids',
                      'base_service_ids',
                      'date_created']
        return self.search_read([('id','in',self.ids)],list_field)[0]
    
    @api.multi
    def create_contract_step1(self):
        ''' 
        '''
        wizard = self.read_wizard()
        print wizard
        self._cr.execute('''delete from apartment_tenant_create_contract_rel where create_contract_id = %s'''%(self.id))
        return {
                'name': 'Create Contract step 1',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'create.contract',
                'view_id': self.env.ref('QL_chung_cu.view_create_contract_wizard_step1').id,
                'type': 'ir.actions.act_window',
                'res_id': self.id,
                'target': 'new'
            }
        
        
    @api.multi
    def create_contract_step2(self):
        ''' Chuyển nhân khẩu chính qua
        '''
        wizard = self.read_wizard()
        self._cr.execute('''delete from apartment_service_create_contract_rel where create_contract_id = %s'''%(self.id))
        
        wizard = self.read_wizard()
        contract = self.temp_contract()
        
        if not wizard['tenant_ids']:
            tenants = contract.create_tenant()
            for x in tenants:
                self._cr.execute('''insert into apartment_tenant_create_contract_rel(create_contract_id,apartment_tenant_id) values(%s,%s)'''%(self.id,x.id))
        
        wizard = self.read_wizard()
        print wizard
        return {
                'name': 'Create Contract step 2',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'create.contract',
                'view_id': self.env.ref('QL_chung_cu.view_create_contract_wizard_step2').id,
                'type': 'ir.actions.act_window',
                'res_id': self.id,
                'target': 'new'
            }
        
    @api.multi
    def create_contract_step3(self):
        ''' Chuyển base service qua
            Check điện nước ban đầu
        '''
        wizard = self.read_wizard()
        if not wizard['tenant_ids']:
            raise except_orm(u'Lỗi!', u'Phải có ít nhất một nhân khẩu trong hợp đồng!')
        contract = self.temp_contract()
        base_service = contract.create_base_service()
        
        
        self.env['apartment.month.index'].check_diennuoc(wizard['room_id'][0],wizard['date_created'],wizard['power_start'],wizard['water_start'])
        
        
        #tenant_obj = self.env['apartment.tenant']
            
        #xedap = self.env.ref('QL_chung_cu.price_giuxedap')
        #xemay = self.env.ref('QL_chung_cu.price_giuxemay')
        
        #xedap_product = self.env['product.product'].browse(xedap.id)
        #xemay_product = self.env['product.product'].browse(xemay.id)
#         for tenant in wizard['tenant_ids']:
#             tenant = tenant_obj.browse(tenant)
#             if tenant.gui_xe == 'xedap':
#                 vals = {
#                         'product_id': xedap_product.id,
#                         'date': wizard['date_start'],
#                         'price': xedap_product.list_price,
#                     }
#                 base_service[0].append(self.env['apartment.service'].create(vals)[0])
#             if tenant.gui_xe == 'xemay':
#                 vals = {
#                         'product_id': xemay_product.id,
#                         'date': wizard['date_start'],
#                         'price': xemay_product.list_price,
#                     }
#                 base_service[0].append(self.env['apartment.service'].create(vals)[0])
                
                
            
        for x in base_service[0]:
            self._cr.execute('''insert into apartment_service_create_contract_rel(create_contract_id,apartment_service_id) values(%s,%s)'''%(self.id,x.id))
            
        print wizard
        return {
                'name': 'Create Contract step 3',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'create.contract',
                'view_id': self.env.ref('QL_chung_cu.view_create_contract_wizard_step3').id,
                'type': 'ir.actions.act_window',
                'res_id': self.id,
                'target': 'new'
            }
        
    @api.multi
    def create_contract_step4(self):
        ''' 
            Show lại toàn bộ dữ liệu đã nhập
        '''
        return {
                'name': 'Create Contract step 4',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'create.contract',
                'view_id': self.env.ref('QL_chung_cu.view_create_contract_wizard_step4').id,
                'type': 'ir.actions.act_window',
                'res_id': self.id,
                'target': 'new'
            }
    
    @api.multi
    def create_contract_step5(self):
        ''' 
            Confirm contract
        '''
        
        wizard = self.read_wizard()
        
        vals = {
                'room_id':wizard['room_id'][0],
                'supplier_id':wizard['supplier_id'][0],
                'deposit':wizard['deposit'],
                'date_start':wizard['date_start'],
                'date_end':wizard['date_end'],
                'gui_xe':wizard['gui_xe'],
                'water_start':wizard['water_start'],
                'power_start':wizard['power_start'],
                'date_created':wizard['date_created'],
                
                'state': 'draft',
                'name': self.env['apartment.contract']._get_name_contract(wizard['date_start'],wizard['room_id'][0])
                
                }
        
        contract = self.env['apartment.contract'].create(vals)
        if wizard['deposit']:
            return {
                    'name': 'Please Validate Invoice',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'account.invoice',
                    'view_id': self.env.ref('account.invoice_form').id,
                    'type': 'ir.actions.act_window',
                    'res_id': contract.invoice_ids[0].id,
                    'target': 'new'
                }
        else:
            return {'type': 'ir.actions.act_window_close'} 
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
